# README #

### What is this repository for? ###

* IlyonAd (Mediation Layer) Module
* Version - 1.0
* (using MoPub as example Mediator)

### How do I get set up? ###

* Dependencies - 
*	For the example : 
*		MoPub Module ; 
*		aol-ad-sdk Module;
* 
* Deployment instructions - 
* 	Import module, and implement methods,
*	according to specifications. 
*	(Instructions set inside each method)

### Contribution guidelines ###

* Guidelines - Please review entire Class first,
*	prior to starting implementation

### Who do I talk to? ###

* Razvan.m@Ilyon.net
* Monetization Team - 07.08.2017